import { Container } from './di/container';
import { Context } from './di/context';
import { FastifyAdapter } from './adapters/fastify';
import { isUndefined } from './utils/shared';
import { UnknownCoreFrameworkError } from './exceptions/unknown-core-framework.exception';
import { Router } from './infrastructure/router';
import { ServerIsNotInitError } from './exceptions/server-not-init.exception.';
import { DefaultErrorHandler } from './infrastructure/error-handler';
import { FrameworkApp, CoreFramework, CoreType, Logger } from './interfaces';

export class App implements FrameworkApp {
  private server: CoreFramework;
  private container: Container;
  private router: Router;
  private coreType: CoreType = CoreType.FASTIFY;
  private appContext: Context;

  constructor() {
    this.appContext = new Context();
  }

  setConfig(config: Record<string, any>) {
    this.appContext.setConfig(config);
  }

  setLogger(logger: Logger) {
    this.appContext.setLogger(logger);
  }

  setCore(type: CoreType) {
    this.coreType = type;
  }

  run(port: number) {
    switch (this.coreType) {
      case CoreType.FASTIFY: {
        this.server = new FastifyAdapter();
      }
    }

    if (isUndefined(this.server)) {
      throw new UnknownCoreFrameworkError();
    }

    this.server.setErrorHandler(DefaultErrorHandler);

    this.router = new Router(this.server);
    this.server.listen(port);
  }

  getDIContainer() {
    if (isUndefined(this.server)) {
      throw new ServerIsNotInitError();
    }

    if (!isUndefined(this.container)) {
      return this.container;
    }

    this.container = new Container(this.appContext, this.router);
    return this.container;
  }

  getRouter() {
    if (isUndefined(this.server)) {
      throw new ServerIsNotInitError();
    }

    return this.router;
  }
}

