export class UnknownCoreFrameworkError extends Error {
  constructor() {
    super('Unknown core framework');
  }
}
