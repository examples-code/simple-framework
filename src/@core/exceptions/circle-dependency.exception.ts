export class CircleDependencyError extends Error {
  constructor() {
    super('CircleDependency');
  }
}
