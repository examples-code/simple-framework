export class ServerIsNotInitError extends Error {
  constructor() {
    super('Server is not init');
  }
}
