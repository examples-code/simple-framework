import { STATUS_CODE } from '../../enums/status-code.enum';
import { HttpException } from './http.exception';

export class BadRequestException extends HttpException {
  constructor() {
    super('BadRequest', STATUS_CODE.BAD_REQUEST);
  }
}
