import { STATUS_CODE } from "src/@core/enums/status-code.enum";

export class HttpException extends Error {
  statusCode: STATUS_CODE = 401;
  constructor(error: string, statusCode: STATUS_CODE) {
    super(error);
    this.statusCode = statusCode;
  }
}
