export class UnknownDependencyError extends Error {
  constructor() {
    super('Unknown dependency');
  }
}
