export const INJECTABLE_METADATA = 'injectable-metadata';
export const CONTROLLER_METADATA = 'controller-metadata';
export const METHOD_METADATA = 'method-metadata';
export const LOGGER_METADATA = 'logger-metadata';
export const CONFIG_METADATA = 'config-metadata';
