import 'reflect-metadata';
import { UnknownDependencyError } from '../exceptions/unknown-dependency.exception';
import { CircleDependencyError } from '../exceptions/circle-dependency.exception';
import { isUndefined } from '../utils/shared';
import { Context } from './context';
import { Router } from '../infrastructure/router';
import {
  CONFIG_METADATA,
  CONTROLLER_METADATA,
  INJECTABLE_METADATA,
  LOGGER_METADATA,
  METHOD_METADATA,
} from '../constants';
import { SCOPE } from '../enums/scope.enum';

interface InjectableData {
  type: any;
  instance?: any;
  scope: SCOPE;
}

export class Container {
  private injectables: Map<string | symbol, InjectableData> = new Map();

  constructor(private appContext: Context, private router: Router) {}

  bind(key: string | symbol, injectable: unknown, scope: SCOPE = SCOPE.TRANSIENT): void {
    this.injectables.set(key, {
      type: injectable,
      scope,
    });
  }

  registerController(injectable: any) {
    const controllerMetadata = Reflect.getMetadata(
      CONTROLLER_METADATA,
      injectable,
    );
    const i = this.loadInstance(injectable);
    const properties = Object.getOwnPropertyNames(injectable.prototype);

    for (const property of properties) {
      if (property == 'contructor') {
        continue;
      }

      const propertyMetadata = Reflect.getMetadata(
        METHOD_METADATA,
        i[property],
      );

      if (!isUndefined(propertyMetadata)) {
        this.router.route(
          propertyMetadata.method,
          controllerMetadata + propertyMetadata.path,
          i[property].bind(i),
        );
      }
    }
  }

  get<T>(key: string | symbol, hierarchy: Set<any> = new Set()): T {
    const injectable = this.injectables.get(key);

    if (isUndefined(injectable)) {
      throw new UnknownDependencyError();
    }

    if (!isUndefined(injectable.instance)) {
      return injectable.instance;
    }

    if (hierarchy.has(injectable.type)) {
      throw new CircleDependencyError();
    }

    hierarchy.add(injectable.type);

    const instance = this.loadInstance<T>(injectable.type, hierarchy);
    if (injectable.scope === SCOPE.SINGLETON) {
      injectable.instance = instance;
      this.injectables.set(key, injectable);
    }

    return instance;
  }

  private loadInstance<T>(type: any, hierarchy: Set<any> = new Set()): T {
    const metadata = Reflect.getMetadata(INJECTABLE_METADATA, type) || [];
    const loggerMetadata = Reflect.getMetadata(LOGGER_METADATA, type) || [];
    const configMetadata = Reflect.getMetadata(CONFIG_METADATA, type) || [];

    const args = [];

    if (!isUndefined(loggerMetadata)) {
      args[loggerMetadata.index] = this.appContext.getLogger();
    }
    if (!isUndefined(configMetadata)) {
      args[configMetadata.index] = this.appContext.getConfig();
    }

    for (const arg of metadata) {
      const key = arg.key;

      const instance = this.get(key, hierarchy);
      args[arg.index] = instance;
    }

    return new type(...args);
  }
}
