import { Logger } from "../interfaces";


export class Context {
  config: any;
  logger: Logger;

  public setConfig(config: any): void {
    this.config = config;
  }

  public setLogger(logger: Logger): void {
    this.logger = logger;
  }

  public getLogger(): Logger {
    return this.logger;
  }

  public getConfig(): void {
    return this.config;
  }
}
