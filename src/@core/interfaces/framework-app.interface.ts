import { Container } from "../di/container";
import { Router } from "../infrastructure/router";
import { CoreType } from "./core-type.interface";
import { Logger } from "./logger.interface";

export interface FrameworkApp {
	setConfig: (config: any) => void;
	setLogger: (logger: Logger) => void;
	setCore: (core: CoreType) => void;
	run: (port: number) => void;
	getDIContainer: () => Container;
	getRouter: () => Router;
}
