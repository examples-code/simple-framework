import { METHOD } from "../enums/method.enum";
import { ErrorHandler, RequestHandler } from "./handler.interface";

export interface CoreFramework {
	listen: (port: number) => void;
	method: (method: METHOD, path: string, cl: RequestHandler) => void;
	setErrorHandler: (handler: ErrorHandler) => void;
  }