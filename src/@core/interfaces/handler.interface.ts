import { Request } from "../infrastructure/request";
import { Response } from "../infrastructure/response";

export type RequestHandler = (
	request: Request,
	response: Response,
) => Record<string, unknown> | string;

export type ErrorHandler = (
	error: Error,
	request: Request,
	response: Response,
) => void;
