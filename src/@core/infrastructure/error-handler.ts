import { HttpException } from '../exceptions/http/http.exception';
import { Request } from './request';
import { Response } from './response';
import { STATUS_CODE } from '../enums/status-code.enum';

export function DefaultErrorHandler(
  error: Error,
  request: Request,
  response: Response,
) {
  if (error instanceof HttpException) {
    response.setStatus(error.statusCode);
  } else {
    response.setStatus(STATUS_CODE.SERVER_ERROR);
  }

  return {
    result: false,
  };
}
