import { FastifyRequest } from 'fastify';
import { METHOD } from '../enums/method.enum';

export class Request {
  private _method: METHOD;
  private _query: Map<string, string | number | boolean> = new Map();
  private _body: Map<string, string | number | boolean> = new Map();
  constructor(req: FastifyRequest) {
    this._method = req.method as METHOD;
    this.parseQuery(req.query);
    if (this.method == 'POST') {
      this.parseBody(req.body);
    }
  }

  private parseQuery(data: Record<string, any>): void {
    for (const [key, value] of Object.entries(data)) {
      this._query.set(key, value);
    }
  }

  private parseBody(data: Record<string, any>): void {
    for (const [key, value] of Object.entries(data)) {
      this._body.set(key, value);
    }
  }

  get method(): METHOD {
    return this._method;
  }

  get body(): Record<string, any> {
    return this._body;
  }

  get query(): Record<string, any> {
    return this._query;
  }
}
