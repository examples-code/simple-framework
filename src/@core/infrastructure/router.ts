import { METHOD } from "../enums/method.enum";
import { CoreFramework, RequestHandler } from "../interfaces";

export class Router {
  constructor(private server: CoreFramework) {}

  get(path: string, h: RequestHandler): void {
    this.route(METHOD.GET, path, h);
  }

  post(path: string, h: RequestHandler): void {
    this.route(METHOD.POST, path, h);
  }

  put(path: string, h: RequestHandler): void {
    this.route(METHOD.PUT, path, h);
  }

  patch(path: string, h: RequestHandler): void {
    this.route(METHOD.PATCH, path, h);
  }

  delete(path: string, h: RequestHandler): void {
    this.route(METHOD.DELETE, path, h);
  }

  route(method: METHOD, path: string, h: RequestHandler): void {
    this.server.method(method, path, h);
  }
}
