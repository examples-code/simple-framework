import { CONTENT_TYPE } from "../enums/content-type.enum";
import { METHOD } from "../enums/method.enum";
import { STATUS_CODE } from "../enums/status-code.enum";


export class Response {
  private contentType: CONTENT_TYPE = CONTENT_TYPE.JSON;
  private status: STATUS_CODE;
  private requestMethod: METHOD;

  constructor(method: METHOD) {
    this.requestMethod = method;
    this.status =
      METHOD.POST == method ? STATUS_CODE.CREATED : STATUS_CODE.SUCCESS;
  }

  setStatus(status: STATUS_CODE): void {
    this.status = status;
  }

  setContentType(type: CONTENT_TYPE): void {
    this.contentType = type;
  }

  getStatus(): STATUS_CODE {
    return this.status;
  }

  getContentType(): CONTENT_TYPE {
    return this.contentType;
  }
}
