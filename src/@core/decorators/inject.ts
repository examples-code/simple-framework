import { INJECTABLE_METADATA } from '../constants';
import { isUndefined } from '../utils/shared';

export function Inject(key: string | symbol) {
  return (target: any, propertyKey: string, index: number) => {
    let properties = Reflect.getMetadata(INJECTABLE_METADATA, target);

    if (isUndefined(properties)) {
      properties = [];
    }

    Reflect.defineMetadata(
      INJECTABLE_METADATA,
      [...properties, { index, key }],
      target,
    );
  };
}
