import { CONTROLLER_METADATA } from '../constants';

export function Controller(route: string) {
  return (target: any) => {
    Reflect.defineMetadata(CONTROLLER_METADATA, route, target);
  };
}
