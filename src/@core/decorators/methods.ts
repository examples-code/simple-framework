import { METHOD_METADATA } from '../constants';
import { METHOD } from '../enums/method.enum';


export function defineMethod(method: METHOD, path: string) {
  return (target: any, propertyKey: string, descriptor: any) => {
    Reflect.defineMetadata(
      METHOD_METADATA,
      {
        path,
        method,
      },
      descriptor.value,
    );
    return descriptor;
  };
}

export function Get(path: string) {
  return defineMethod(METHOD.GET, path);
}

export function Post(path: string) {
  return defineMethod(METHOD.POST, path);
}

export function Put(path: string) {
  return defineMethod(METHOD.PUT, path);
}
