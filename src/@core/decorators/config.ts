import { CONFIG_METADATA } from '../constants';

export function Config() {
  return (target: any, key: string, index: number) => {
    Reflect.defineMetadata(
      CONFIG_METADATA,
      {
        index,
      },
      target,
    );
  };
}
