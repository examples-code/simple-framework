import { LOGGER_METADATA } from '../constants';

export function Logger() {
  return (target: any, key: string, index: number) => {
    Reflect.defineMetadata(
      LOGGER_METADATA,
      {
        index,
      },
      target,
    );
  };
}
