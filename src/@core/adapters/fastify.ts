import Fastify, {
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
  HTTPMethods,
} from 'fastify';
import { RequestHandler, ErrorHandler, CoreFramework } from '../interfaces';
import { Request } from '../infrastructure/request';
import { Response } from '../infrastructure/response';
import { METHOD } from '../enums/method.enum';

export class FastifyAdapter implements CoreFramework {
  server: FastifyInstance;

  constructor() {
    this.server = Fastify();
  }

  setErrorHandler(h: ErrorHandler): void {
    this.server.setErrorHandler(function (error, request, reply) {
      const responseInstance = new Response(request.method as METHOD);
      const requestInstance = new Request(request);
      const result = h(error, requestInstance, responseInstance);
      reply.statusCode = responseInstance.getStatus();
      reply.send(result);
    });
  }

  listen(port: number): void {
    this.server.listen(port);
  }

  method(method: METHOD, path: string, h: RequestHandler): void {
    this.server.route({
      url: this.preparePath(path),
      method: method.toUpperCase() as HTTPMethods,
      handler: this.handler(h),
    });
  }

  private preparePath(path: string): string {
    if (path.endsWith('/')) {
      return path.slice(0, path.length - 1);
    }
    return path;
  }

  private handler(cl: RequestHandler) {
    return (request: FastifyRequest, reply: FastifyReply) => {
      const responseInstance = new Response(request.method as METHOD);
      const requestInstance = new Request(request);
      const result = cl(requestInstance, responseInstance);
      reply.statusCode = responseInstance.getStatus();
      reply.send(result);
    };
  }
}
