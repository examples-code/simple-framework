import {
  Get,
  Request,
  Response,
  Controller,
  Inject,
} from '../../../@core';
import { NewsService } from '../services/news.service';
import { NEWS_SERVICE } from '../injectable-types';
import { Post } from '../interfaces/post';

@Controller('/app')
export class AppController {
  constructor(@Inject(NEWS_SERVICE) private newsService: NewsService) {}

  @Get('/')
  list(request: Request, response: Response): Post[] {
    return this.newsService.list();
  }
}
