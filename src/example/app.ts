import * as core from '../@core/app';
import { AppController } from './api/controllers/app.controller';
import { NewsService } from './api/services/news.service';
import { NEWS_SERVICE } from './api/injectable-types';
import configuration from './api/configuration';

const app = new core.App();

app.setConfig(configuration);

app.run(3000);

const container = app.getDIContainer();

container.bind(NEWS_SERVICE, NewsService);
container.registerController(AppController);
